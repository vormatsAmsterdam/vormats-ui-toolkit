"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "SVGIcon", {
  enumerable: true,
  get: function () {
    return _SVGIcon.default;
  }
});
Object.defineProperty(exports, "PlayButtonIcon", {
  enumerable: true,
  get: function () {
    return _VideoControls.PlayButtonIcon;
  }
});
Object.defineProperty(exports, "VolumeIcon", {
  enumerable: true,
  get: function () {
    return _VideoControls.VolumeIcon;
  }
});
Object.defineProperty(exports, "FullscreenIcon", {
  enumerable: true,
  get: function () {
    return _VideoControls.FullscreenIcon;
  }
});
Object.defineProperty(exports, "DownloadIcon", {
  enumerable: true,
  get: function () {
    return _VideoControls.DownloadIcon;
  }
});
Object.defineProperty(exports, "MuteIcon", {
  enumerable: true,
  get: function () {
    return _VideoControls.MuteIcon;
  }
});
Object.defineProperty(exports, "PauseIcon", {
  enumerable: true,
  get: function () {
    return _VideoControls.PauseIcon;
  }
});
Object.defineProperty(exports, "SearchIcon", {
  enumerable: true,
  get: function () {
    return _Generic.SearchIcon;
  }
});

var _SVGIcon = _interopRequireDefault(require("./SVGIcon"));

var _VideoControls = require("./SVGIcons/VideoControls");

var _Generic = require("./SVGIcons/Generic");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }