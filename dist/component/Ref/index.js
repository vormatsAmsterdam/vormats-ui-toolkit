"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function () {
    return _Ref.default;
  }
});

var _Ref = _interopRequireDefault(require("./Ref"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }