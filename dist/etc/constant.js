"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sizing = void 0;
const sizing = {
  XS: 'xs',
  SM: 'sm',
  MD: 'md',
  LG: 'lg',
  XL: 'xl'
};
exports.sizing = sizing;