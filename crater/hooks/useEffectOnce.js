"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = useEffectOnce;

var _react = require("react");

function useEffectOnce(effect) {
  (0, _react.useEffect)(effect, []);
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jcmF0ZXIvaG9va3MvdXNlRWZmZWN0T25jZS5qcyJdLCJuYW1lcyI6WyJ1c2VFZmZlY3RPbmNlIiwiZWZmZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBRWUsU0FBU0EsYUFBVCxDQUF1QkMsTUFBdkIsRUFBK0I7QUFDNUMsd0JBQVVBLE1BQVYsRUFBa0IsRUFBbEI7QUFDRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHVzZUVmZmVjdCB9IGZyb20gJ3JlYWN0JztcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gdXNlRWZmZWN0T25jZShlZmZlY3QpIHtcbiAgdXNlRWZmZWN0KGVmZmVjdCwgW10pO1xufVxuIl19