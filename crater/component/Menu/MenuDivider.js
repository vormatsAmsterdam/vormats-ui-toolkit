"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _MenuModule = _interopRequireDefault(require("./Menu.module.scss"));

var _this = void 0;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }

const MenuDivider = function MenuDivider() {
  _newArrowCheck(this, _this);

  return _react.default.createElement("hr", {
    "data-react-toolbox": "menu-divider",
    className: _MenuModule.default.menuDivider
  });
}.bind(void 0);

var _default = MenuDivider;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jcmF0ZXIvY29tcG9uZW50L01lbnUvTWVudURpdmlkZXIuanMiXSwibmFtZXMiOlsiTWVudURpdmlkZXIiLCJzdHlsZXMiLCJtZW51RGl2aWRlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOzs7Ozs7OztBQUVBLE1BQU1BLFdBQVcsR0FBRztBQUFBOztBQUFBLFNBQU07QUFBSSwwQkFBbUIsY0FBdkI7QUFBc0MsSUFBQSxTQUFTLEVBQUVDLG9CQUFPQztBQUF4RCxJQUFOO0FBQUEsQ0FBSCxhQUFqQjs7ZUFFZUYsVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgc3R5bGVzIGZyb20gJy4vTWVudS5tb2R1bGUuc2Nzcyc7XG5cbmNvbnN0IE1lbnVEaXZpZGVyID0gKCkgPT4gPGhyIGRhdGEtcmVhY3QtdG9vbGJveD1cIm1lbnUtZGl2aWRlclwiIGNsYXNzTmFtZT17c3R5bGVzLm1lbnVEaXZpZGVyfSAvPjtcblxuZXhwb3J0IGRlZmF1bHQgTWVudURpdmlkZXI7XG4iXX0=