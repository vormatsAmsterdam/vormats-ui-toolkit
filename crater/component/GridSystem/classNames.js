"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getClass;

var _GridSystemModule = _interopRequireDefault(require("./GridSystem.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getClass(className) {
  return _GridSystemModule.default && _GridSystemModule.default[className] ? _GridSystemModule.default[className] : className;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jcmF0ZXIvY29tcG9uZW50L0dyaWRTeXN0ZW0vY2xhc3NOYW1lcy5qcyJdLCJuYW1lcyI6WyJnZXRDbGFzcyIsImNsYXNzTmFtZSIsInN0eWxlcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7O0FBRWUsU0FBU0EsUUFBVCxDQUFrQkMsU0FBbEIsRUFBNkI7QUFDMUMsU0FBT0MsNkJBQVVBLDBCQUFPRCxTQUFQLENBQVYsR0FBOEJDLDBCQUFPRCxTQUFQLENBQTlCLEdBQWtEQSxTQUF6RDtBQUNEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHN0eWxlcyBmcm9tICcuL0dyaWRTeXN0ZW0ubW9kdWxlLnNjc3MnO1xuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBnZXRDbGFzcyhjbGFzc05hbWUpIHtcbiAgcmV0dXJuIHN0eWxlcyAmJiBzdHlsZXNbY2xhc3NOYW1lXSA/IHN0eWxlc1tjbGFzc05hbWVdIDogY2xhc3NOYW1lO1xufVxuIl19