"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getRowProps = getRowProps;
exports.default = Row;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classNames = _interopRequireDefault(require("../classNames"));

var _createProps = _interopRequireDefault(require("../createProps"));

var _types = require("../types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const rowKeys = ['start', 'center', 'end', 'top', 'middle', 'bottom', 'around', 'between'];
const propTypes = {
  reverse: _propTypes.default.bool,
  compact: _propTypes.default.bool,
  noGutterCompensation: _propTypes.default.bool,
  start: _types.ViewportSizeType,
  center: _types.ViewportSizeType,
  end: _types.ViewportSizeType,
  top: _types.ViewportSizeType,
  middle: _types.ViewportSizeType,
  bottom: _types.ViewportSizeType,
  around: _types.ViewportSizeType,
  between: _types.ViewportSizeType,
  className: _propTypes.default.string,
  tagName: _propTypes.default.string,
  children: _propTypes.default.node
};

function getRowClassNames(props) {
  const modificators = [props.className, (0, _classNames.default)('row')];

  for (let i = 0; i < rowKeys.length; ++i) {
    const key = rowKeys[i];
    const value = props[key];

    if (value) {
      modificators.push((0, _classNames.default)(`${key}-${value}`));
    }
  }

  if (props.reverse) {
    modificators.push((0, _classNames.default)('reverse'));
  }

  if (props.compact) {
    modificators.push((0, _classNames.default)('compact'));
  }

  if (props.noGutterCompensation) {
    modificators.push((0, _classNames.default)('noGutterCompensation'));
  }

  return modificators;
}

function getRowProps(props) {
  return (0, _createProps.default)(propTypes, props, getRowClassNames(props));
}

function Row(props) {
  const {
    tagName
  } = props;
  return _react.default.createElement(tagName || 'div', getRowProps(props));
}

Row.propTypes = propTypes;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jcmF0ZXIvY29tcG9uZW50L0dyaWRTeXN0ZW0vY29tcG9uZW50cy9Sb3cuanMiXSwibmFtZXMiOlsicm93S2V5cyIsInByb3BUeXBlcyIsInJldmVyc2UiLCJQcm9wVHlwZXMiLCJib29sIiwiY29tcGFjdCIsIm5vR3V0dGVyQ29tcGVuc2F0aW9uIiwic3RhcnQiLCJWaWV3cG9ydFNpemVUeXBlIiwiY2VudGVyIiwiZW5kIiwidG9wIiwibWlkZGxlIiwiYm90dG9tIiwiYXJvdW5kIiwiYmV0d2VlbiIsImNsYXNzTmFtZSIsInN0cmluZyIsInRhZ05hbWUiLCJjaGlsZHJlbiIsIm5vZGUiLCJnZXRSb3dDbGFzc05hbWVzIiwicHJvcHMiLCJtb2RpZmljYXRvcnMiLCJpIiwibGVuZ3RoIiwia2V5IiwidmFsdWUiLCJwdXNoIiwiZ2V0Um93UHJvcHMiLCJSb3ciLCJSZWFjdCIsImNyZWF0ZUVsZW1lbnQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7QUFFQSxNQUFNQSxPQUFPLEdBQUcsQ0FBQyxPQUFELEVBQVUsUUFBVixFQUFvQixLQUFwQixFQUEyQixLQUEzQixFQUFrQyxRQUFsQyxFQUE0QyxRQUE1QyxFQUFzRCxRQUF0RCxFQUFnRSxTQUFoRSxDQUFoQjtBQUVBLE1BQU1DLFNBQVMsR0FBRztBQUNoQkMsRUFBQUEsT0FBTyxFQUFFQyxtQkFBVUMsSUFESDtBQUVoQkMsRUFBQUEsT0FBTyxFQUFFRixtQkFBVUMsSUFGSDtBQUdoQkUsRUFBQUEsb0JBQW9CLEVBQUVILG1CQUFVQyxJQUhoQjtBQUloQkcsRUFBQUEsS0FBSyxFQUFFQyx1QkFKUztBQUtoQkMsRUFBQUEsTUFBTSxFQUFFRCx1QkFMUTtBQU1oQkUsRUFBQUEsR0FBRyxFQUFFRix1QkFOVztBQU9oQkcsRUFBQUEsR0FBRyxFQUFFSCx1QkFQVztBQVFoQkksRUFBQUEsTUFBTSxFQUFFSix1QkFSUTtBQVNoQkssRUFBQUEsTUFBTSxFQUFFTCx1QkFUUTtBQVVoQk0sRUFBQUEsTUFBTSxFQUFFTix1QkFWUTtBQVdoQk8sRUFBQUEsT0FBTyxFQUFFUCx1QkFYTztBQVloQlEsRUFBQUEsU0FBUyxFQUFFYixtQkFBVWMsTUFaTDtBQWFoQkMsRUFBQUEsT0FBTyxFQUFFZixtQkFBVWMsTUFiSDtBQWNoQkUsRUFBQUEsUUFBUSxFQUFFaEIsbUJBQVVpQjtBQWRKLENBQWxCOztBQWlCQSxTQUFTQyxnQkFBVCxDQUEwQkMsS0FBMUIsRUFBaUM7QUFDL0IsUUFBTUMsWUFBWSxHQUFHLENBQUNELEtBQUssQ0FBQ04sU0FBUCxFQUFrQix5QkFBUyxLQUFULENBQWxCLENBQXJCOztBQUVBLE9BQUssSUFBSVEsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR3hCLE9BQU8sQ0FBQ3lCLE1BQTVCLEVBQW9DLEVBQUVELENBQXRDLEVBQXlDO0FBQ3ZDLFVBQU1FLEdBQUcsR0FBRzFCLE9BQU8sQ0FBQ3dCLENBQUQsQ0FBbkI7QUFDQSxVQUFNRyxLQUFLLEdBQUdMLEtBQUssQ0FBQ0ksR0FBRCxDQUFuQjs7QUFDQSxRQUFJQyxLQUFKLEVBQVc7QUFDVEosTUFBQUEsWUFBWSxDQUFDSyxJQUFiLENBQWtCLHlCQUFVLEdBQUVGLEdBQUksSUFBR0MsS0FBTSxFQUF6QixDQUFsQjtBQUNEO0FBQ0Y7O0FBRUQsTUFBSUwsS0FBSyxDQUFDcEIsT0FBVixFQUFtQjtBQUNqQnFCLElBQUFBLFlBQVksQ0FBQ0ssSUFBYixDQUFrQix5QkFBUyxTQUFULENBQWxCO0FBQ0Q7O0FBRUQsTUFBSU4sS0FBSyxDQUFDakIsT0FBVixFQUFtQjtBQUNqQmtCLElBQUFBLFlBQVksQ0FBQ0ssSUFBYixDQUFrQix5QkFBUyxTQUFULENBQWxCO0FBQ0Q7O0FBRUQsTUFBSU4sS0FBSyxDQUFDaEIsb0JBQVYsRUFBZ0M7QUFDOUJpQixJQUFBQSxZQUFZLENBQUNLLElBQWIsQ0FBa0IseUJBQVMsc0JBQVQsQ0FBbEI7QUFDRDs7QUFFRCxTQUFPTCxZQUFQO0FBQ0Q7O0FBRU0sU0FBU00sV0FBVCxDQUFxQlAsS0FBckIsRUFBNEI7QUFDakMsU0FBTywwQkFBWXJCLFNBQVosRUFBdUJxQixLQUF2QixFQUE4QkQsZ0JBQWdCLENBQUNDLEtBQUQsQ0FBOUMsQ0FBUDtBQUNEOztBQUVjLFNBQVNRLEdBQVQsQ0FBYVIsS0FBYixFQUFvQjtBQUNqQyxRQUFNO0FBQUVKLElBQUFBO0FBQUYsTUFBY0ksS0FBcEI7QUFDQSxTQUFPUyxlQUFNQyxhQUFOLENBQW9CZCxPQUFPLElBQUksS0FBL0IsRUFBc0NXLFdBQVcsQ0FBQ1AsS0FBRCxDQUFqRCxDQUFQO0FBQ0Q7O0FBRURRLEdBQUcsQ0FBQzdCLFNBQUosR0FBZ0JBLFNBQWhCIiwic291cmNlc0NvbnRlbnQiOlsiLyogZXNsaW50LWRpc2FibGUgcmVhY3Qvbm8tdW51c2VkLXByb3AtdHlwZXMgKi9cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGdldENsYXNzIGZyb20gJy4uL2NsYXNzTmFtZXMnO1xuaW1wb3J0IGNyZWF0ZVByb3BzIGZyb20gJy4uL2NyZWF0ZVByb3BzJztcbmltcG9ydCB7IFZpZXdwb3J0U2l6ZVR5cGUgfSBmcm9tICcuLi90eXBlcyc7XG5cbmNvbnN0IHJvd0tleXMgPSBbJ3N0YXJ0JywgJ2NlbnRlcicsICdlbmQnLCAndG9wJywgJ21pZGRsZScsICdib3R0b20nLCAnYXJvdW5kJywgJ2JldHdlZW4nXTtcblxuY29uc3QgcHJvcFR5cGVzID0ge1xuICByZXZlcnNlOiBQcm9wVHlwZXMuYm9vbCxcbiAgY29tcGFjdDogUHJvcFR5cGVzLmJvb2wsXG4gIG5vR3V0dGVyQ29tcGVuc2F0aW9uOiBQcm9wVHlwZXMuYm9vbCxcbiAgc3RhcnQ6IFZpZXdwb3J0U2l6ZVR5cGUsXG4gIGNlbnRlcjogVmlld3BvcnRTaXplVHlwZSxcbiAgZW5kOiBWaWV3cG9ydFNpemVUeXBlLFxuICB0b3A6IFZpZXdwb3J0U2l6ZVR5cGUsXG4gIG1pZGRsZTogVmlld3BvcnRTaXplVHlwZSxcbiAgYm90dG9tOiBWaWV3cG9ydFNpemVUeXBlLFxuICBhcm91bmQ6IFZpZXdwb3J0U2l6ZVR5cGUsXG4gIGJldHdlZW46IFZpZXdwb3J0U2l6ZVR5cGUsXG4gIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgdGFnTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgY2hpbGRyZW46IFByb3BUeXBlcy5ub2RlLFxufTtcblxuZnVuY3Rpb24gZ2V0Um93Q2xhc3NOYW1lcyhwcm9wcykge1xuICBjb25zdCBtb2RpZmljYXRvcnMgPSBbcHJvcHMuY2xhc3NOYW1lLCBnZXRDbGFzcygncm93JyldO1xuXG4gIGZvciAobGV0IGkgPSAwOyBpIDwgcm93S2V5cy5sZW5ndGg7ICsraSkge1xuICAgIGNvbnN0IGtleSA9IHJvd0tleXNbaV07XG4gICAgY29uc3QgdmFsdWUgPSBwcm9wc1trZXldO1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgbW9kaWZpY2F0b3JzLnB1c2goZ2V0Q2xhc3MoYCR7a2V5fS0ke3ZhbHVlfWApKTtcbiAgICB9XG4gIH1cblxuICBpZiAocHJvcHMucmV2ZXJzZSkge1xuICAgIG1vZGlmaWNhdG9ycy5wdXNoKGdldENsYXNzKCdyZXZlcnNlJykpO1xuICB9XG5cbiAgaWYgKHByb3BzLmNvbXBhY3QpIHtcbiAgICBtb2RpZmljYXRvcnMucHVzaChnZXRDbGFzcygnY29tcGFjdCcpKTtcbiAgfVxuXG4gIGlmIChwcm9wcy5ub0d1dHRlckNvbXBlbnNhdGlvbikge1xuICAgIG1vZGlmaWNhdG9ycy5wdXNoKGdldENsYXNzKCdub0d1dHRlckNvbXBlbnNhdGlvbicpKTtcbiAgfVxuXG4gIHJldHVybiBtb2RpZmljYXRvcnM7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRSb3dQcm9wcyhwcm9wcykge1xuICByZXR1cm4gY3JlYXRlUHJvcHMocHJvcFR5cGVzLCBwcm9wcywgZ2V0Um93Q2xhc3NOYW1lcyhwcm9wcykpO1xufVxuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBSb3cocHJvcHMpIHtcbiAgY29uc3QgeyB0YWdOYW1lIH0gPSBwcm9wcztcbiAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQodGFnTmFtZSB8fCAnZGl2JywgZ2V0Um93UHJvcHMocHJvcHMpKTtcbn1cblxuUm93LnByb3BUeXBlcyA9IHByb3BUeXBlcztcbiJdfQ==