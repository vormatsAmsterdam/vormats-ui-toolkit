/* eslint-disable import/no-extraneous-dependencies */
import '@storybook/addon-actions/register';
import '@storybook/addon-a11y/register';
import '@storybook/addon-viewport/register';
import '@storybook/addon-cssresources/register';
import '@storybook/addon-options/register';
import '@storybook/addon-knobs/register';
