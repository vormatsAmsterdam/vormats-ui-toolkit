import { create } from '@storybook/theming/dist/create';

export default create({
  base: 'light',

  brandTitle: 'Crate UI',
  brandUrl: 'https://ui.accordium.com',
  brandImage:
    'https://static.accordium.com/16f814fdfac20ea32e372116e12aba57ee1d38df4ec067fa6de169d2e0fa565f/e649a642-bedb-4f52-bb1c-98f5334bfa2d.png',
});
// big 'https://static.accordium.com/16f814fdfac20ea32e372116e12aba57ee1d38df4ec067fa6de169d2e0fa565f/d697bfed-6fc9-4c07-8b10-e9b14ca6046e.png'
