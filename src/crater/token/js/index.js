const token = {
  fontSize: {
    tiny: { value: '10px' },
    small: { value: '12px' },
    medium: { value: '14px' },
    large: { value: '16px' },
    xl: { value: '18px' },
    xxl: { value: '20px' },
    xxxl: { value: '24px' },
    base: { value: '14px' },
  },
  size: {
    XS: 'xs',
    SM: 'sm',
    MD: 'md',
    LG: 'lg',
    XL: 'xl',
  },
  imageSize: {
    XXXS: 'xxxs',
    XXS: 'xxs',
    XS: 'xs',
    SM: 'sm',
    MD: 'md',
    LG: 'lg',
    XL: 'xl',
    XXL: 'xxl',
    XXXL: 'xxxl',
  },
  color: {
    DEFAULT: '#3A3D4A',
    PRIMARY: '#27ADFF',
    DANGER: '#FF7D7A',
    BRAND: '#FFC145',
    WHITE: '#FFFFFF',
  },
};

export default token;
