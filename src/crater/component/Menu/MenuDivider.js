import React from 'react';
import styles from './Menu.module.scss';

const MenuDivider = () => <hr data-react-toolbox="menu-divider" className={styles.menuDivider} />;

export default MenuDivider;
